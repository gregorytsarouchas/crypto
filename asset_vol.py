from binance.client import Client
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

import math



client = Client(api_key='api key',
                api_secret='api secret')
info = client.get_account()
balances = info['balances']


def volatility(X):
 holding_pairs_with_usdt_or_busd=[]
 i = client.get_historical_klines(symbol=str(X), interval='1d', start_str='2016.01.01', end_str='2021.03.31')
 i_df = pd.DataFrame(i, columns=['Open time', 'Open', 'High', 'Low', 'Close', 'Volume', 'Close time',
                                'Quote asset volume', 'Number of trades', 'Taker buy base asset volume',
                                'Taker buy quote asset volume', 'Ignore'])
 i_df['Open time'] = pd.to_datetime(i_df['Open time'], unit='ms')
 i_df.set_index('Open time', inplace=True)
 i_df['Close'] = i_df['Close'].astype(float)
 i_df['Open'] = i_df['Open'].astype(float)


 TRADING_DAYS = 365 #tha einai xrisimo se crypto poy den exoyn vgei poly kairo
 returns = np.log(i_df['Close']/i_df['Close'].shift(1))
 returns.fillna(0, inplace=True)
 print(returns.describe())
 volatility = returns.rolling(window=30).std()*100
 fig = plt.figure(figsize=(13, 7))
 ax1 = fig.add_subplot(1, 1, 1)
 volatility.plot(ax=ax1)
 ax1.set_xlabel('Date')
 ax1.set_ylabel('Volatility')
 ax1.set_title(str(X)+' Volatility')
 return plt.show()
volatility('BTCUSDT')

