from binance.client import Client
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import datetime as dt
import math
from statsmodels.graphics import tsaplots
from scipy.stats import stats,norm

today,start=dt.datetime.today(),dt.datetime(2021,1,1)
start,end= start.strftime("%Y-%m-%d"),today.strftime("%Y-%m-%d")





client = Client(api_key='api_key',
                api_secret='api_secret')
info = client.get_account()
balances = info['balances']

tickers=['BNBUSDT', 'ADAUSDT', 'XLMUSDT', 'VTHOUSDT', 'RENUSDT', 'COTIUSDT', 'DOTUSDT']#na pairnei ta holding_tickers_usdt_or_busd apo to crypto_best
volatility_data=pd.DataFrame()
def vol(tickers):
 k=0
 for j in tickers:
  i = client.get_historical_klines(symbol=str(j), interval='1h', start_str=start, end_str=end)
  i_df = pd.DataFrame(i, columns=['Open time', 'Open', 'High', 'Low', 'Close', 'Volume', 'Close time',
                                 'Quote asset volume', 'Number of trades', 'Taker buy base asset volume',
                                 'Taker buy quote asset volume', 'Ignore'])
  i_df['Open time'] = pd.to_datetime(i_df['Open time'], unit='ms')
  i_df.set_index('Open time', inplace=True)
  i_df['Close'] = i_df['Close'].astype(float)
  i_df['Open'] = i_df['Open'].astype(float)
  returns = np.log(i_df['Close']/i_df['Close'].shift(1))
  returns.fillna(0, inplace=True)
  ticker_pair=str(j)
  volatility_data.insert(k,ticker_pair,returns.rolling(window=30).std()*100)
  k+=1
 volatility_data.fillna(0,inplace=True)
 fig = plt.figure(figsize=(13, 7))
 ax1 = fig.add_subplot(1, 1, 1)
 volatility_data.plot(ax=ax1)
 ax1.set_xlabel('Date')
 ax1.set_ylabel('Volatility')
 plt.show()
 print(volatility_data)





print(vol(tickers))


#values = returns[1:]  # skip first NA value
#x = np.linspace(values.min(), values.max(), len(values))
#loc, scale = norm.fit(values)
#param_density = norm.pdf(x, loc=loc, scale=scale)
#label = 'mean=%.4f, std=%.4f' % (loc, scale)
#fig, ax = plt.subplots(figsize=(10, 6))
#ax.hist(values, bins=30)
#ax.plot(x, param_density, 'r-', label=label)
#ax.legend(loc='best')
#plt.show()
