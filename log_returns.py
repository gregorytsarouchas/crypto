from binance.client import Client
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns


client = Client(api_key='api_key',
                api_secret='api_secret')
info = client.get_account()
balances = info['balances']




Closing_data=pd.DataFrame()
k=0
tickers=['BTCUSDT','ETHUSDT']
for j in tickers:
 i = client.get_historical_klines(symbol=str(j), interval='1d', start_str='2020.01.01', end_str='2021.05.03')
 i_df = pd.DataFrame(i, columns=['Open time', 'Open', 'High', 'Low', 'Close', 'Volume', 'Close time',
                                'Quote asset volume', 'Number of trades', 'Taker buy base asset volume',
                                'Taker buy quote asset volume', 'Ignore'])
 i_df['Open time'] = pd.to_datetime(i_df['Open time'], unit='ms')
 i_df.set_index('Open time', inplace=True)
 i_df['Close'] = i_df['Close'].astype(float)
 i_df['Open'] = i_df['Open'].astype(float)
 x=str(j)
 Closing_data.insert(int(k),x,i_df['Close'])
 k+=1
Closing_data.fillna(0, inplace=True)
print(Closing_data)
returns=Closing_data.pct_change()

 #print(i_df.head())
#i_df.to_excel (r'C:\Users\Grts2\PycharmProjects\mg\output.xlsx', index = False, header=True)
returns =pd.DataFrame(np.log(i_df['Close']/i_df['Close'].shift(1)))

returns.fillna(0, inplace=True)
returns=returns.rename(columns={'Close': 'BTCUSDT'})
print(returns)
returns.plot()
plt.show()
