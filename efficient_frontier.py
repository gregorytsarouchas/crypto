from binance.client import Client
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
import datetime as dt


start,today=dt.datetime(2021,1,1),dt.datetime.today()
start,end= start.strftime("%Y-%m-%d"),today.strftime("%Y-%m-%d")


client = Client(api_key='api_key',
                api_secret='api_secret')

def decimal_str(x: float, decimals: int = 10) -> str:         #Scientific notation to decimal format
    return format(x, f".{decimals}f").lstrip().rstrip('0')


info = client.get_account()
balances = info['balances'] #balances=[{'asset': 'xxx', 'free': 'xxxxx', 'locked': 'xxxx'..........}
assets = []
values = []

def portfolio(balances):
 for i in range(len(balances)):
     for key in balances[i]:
         if key == 'asset':
             assets.append(balances[i][key])
         if key == 'free':
             values.append(balances[i][key])
             if info['balances'][i]['locked'] !=0.00000000:
               values[-1]=float(values[-1]) +float(balances[i]['locked'])


 dic=dict(zip(assets,values))#before removing zeros  form :{'BTC': 0.0, 'LTC': 0.0,,,,,,,,,}
 portfolio = {} #after removing zeros
 for x, y in dic.items():
     if y != 0:
         portfolio[x] = y
 portfolio.pop('BCHSV')#delisted
 holding_tickers=[]
 ammount_of_tickers=[]

 for x,y in portfolio.items():
     holding_tickers.append(x)
     ammount_of_tickers.append(y)
 prices=[]

 for i in holding_tickers:
    try:
        X = i + 'USDT'
        prices.append(client.get_ticker(symbol=X))
    except :
        X = i + 'BUSD'
        prices.append(client.get_ticker(symbol=X))
 Last_price_of_tickers=[]
 for i in prices :
     Last_price_of_tickers.append(i['lastPrice'])

 estimated_value_in_usdt = []
 for i in range(0, len(Last_price_of_tickers)):
     estimated_value_in_usdt.append(float(Last_price_of_tickers[i]) * ammount_of_tickers[i])

 suma= sum(estimated_value_in_usdt)
 weights=[]
 for i in estimated_value_in_usdt:
     weights.append(decimal_str((i/suma)*100))


#creating dataframe
 tickers = {'ammount': ammount_of_tickers}
 data=pd.DataFrame(tickers,index=holding_tickers)
 data.insert(1, "Estimated Value in usdt", estimated_value_in_usdt)
 data.insert(2, "Weights", weights)
 return data,holding_tickers,weights

data,holding_tickers,weights=portfolio(balances)
print(data)
print('assets=',holding_tickers)


def find_correlation_matrix(holding_tickers):
 global holding_pairs_with_usdt_or_busd,coins
 series = []
 holding_pairs_with_usdt_or_busd=[]

 for i in holding_tickers:
     X = i + 'USDT'
     try:

         i = client.get_historical_klines(symbol=str(X), interval='1d', start_str=start, end_str=end)
         i_df = pd.DataFrame(i, columns=['Open time', 'Open', 'High', 'Low', 'Close', 'Volume', 'Close time',
                                         'Quote asset volume', 'Number of trades', 'Taker buy base asset volume',
                                         'Taker buy quote asset volume', 'Ignore'])
         i_df['Open time'] = pd.to_datetime(i_df['Open time'], unit='ms')
         i_df.set_index('Open time', inplace=True)
         i_df['Close'] = i_df['Close'].astype(float)
         series.append(i_df['Close'])
         holding_pairs_with_usdt_or_busd.append(X)
     except:
         X = i + 'BUSD'
         i = client.get_historical_klines(symbol=str(X), interval='1d', start_str=start, end_str=end)
         i_df = pd.DataFrame(i, columns=['Open time', 'Open', 'High', 'Low', 'Close', 'Volume', 'Close time',
                                        'Quote asset volume', 'Number of trades', 'Taker buy base asset volume',
                                        'Taker buy quote asset volume', 'Ignore'])
         i_df['Open time'] = pd.to_datetime(i_df['Open time'], unit='ms')
         i_df.set_index('Open time', inplace=True)
         i_df['Close'] = i_df['Close'].astype(float)
         series.append(i_df['Close'])
         holding_pairs_with_usdt_or_busd.append(X)

 coins = pd.concat(series, axis=1)
 coins.columns = holding_tickers
 sns.heatmap(coins.corr(), annot=True)
 plt.show()
 print('holding_pairs_in_binance=',holding_pairs_with_usdt_or_busd)



print(find_correlation_matrix(holding_tickers))
#print(holding_pairs_with_usdt_or_busd)
print(coins) #dataframe with prices of each asset







print('-'*65)



Closing_data=pd.DataFrame()
k=0
for j in holding_pairs_with_usdt_or_busd:
 i = client.get_historical_klines(symbol=str(j), interval='1d', start_str=start, end_str=end)
 i_df = pd.DataFrame(i, columns=['Open time', 'Open', 'High', 'Low', 'Close', 'Volume', 'Close time',
                                'Quote asset volume', 'Number of trades', 'Taker buy base asset volume',
                                'Taker buy quote asset volume', 'Ignore'])
 i_df['Open time'] = pd.to_datetime(i_df['Open time'], unit='ms')
 i_df.set_index('Open time', inplace=True)
 i_df['Close'] = i_df['Close'].astype(float)
 i_df['Open'] = i_df['Open'].astype(float)
 x=str(j)
 Closing_data.insert(int(k),x,i_df['Close'])
 k+=1
Closing_data.fillna(0, inplace=True)
returns=Closing_data.pct_change()
returns.fillna(0,inplace=True)


print('returns=',returns)
print(returns.describe())
std_returns=returns.describe().loc['std']
mean_returns=returns.mean()
cov_matrix=returns.cov()
print(cov_matrix)
weights=[float(i) for i in weights]
weights=np.array(weights)

port_variance=np.dot(weights.T,np.dot(cov_matrix,weights))
print('Portfolio variance is',port_variance)
port_volatility=np.sqrt(port_variance)
print(port_volatility)
print('mean_returns\n',mean_returns)
print('-'*40)
print('std of retunrs\n',std_returns)



def portfolio_annualised_performance(weights, mean_returns, cov_matrix):
    returns = np.sum(mean_returns*weights ) *365
    std = np.sqrt(np.dot(weights.T, np.dot(cov_matrix, weights))) * np.sqrt(365)
    return std, returns
print(portfolio_annualised_performance(weights,mean_returns,cov_matrix))


#We will generate 10.000 portfolios and we will use the interest rate on a three-month U.S. Treasury bill
risk_free_rate=0.0130
num_of_portfolios=10000


def random_portfolios(num_of_portfolios,mean_returns,cov_matrix,risk_free_rate):
 random_data=np.zeros((3,num_of_portfolios))
 weights_rellocation=[]
 for i in range(num_of_portfolios):
     weights = np.random.random(len(holding_tickers))
     weights /= np.sum(weights)
     weights_rellocation.append(weights)
     portfolio_std,portfolio_return = portfolio_annualised_performance(weights,mean_returns,cov_matrix)
     sharpe_ratio = (portfolio_return-risk_free_rate) /portfolio_std
     random_data[0, i] = portfolio_std
     random_data[1, i] = portfolio_return
     random_data[2, i] = sharpe_ratio
 return random_data,weights_rellocation
#print(random_portfolios(num_of_portfolios,mean_returns,cov_matrix,risk_free_rate))



def efficient_frontier(mean_returns,cov_matrix,num_of_portfolios,risk_free_rate):
    results, weights = random_portfolios(num_of_portfolios, mean_returns, cov_matrix, risk_free_rate)

    max_sharpe_ratio = np.argmax(results[2])
    sdp, rp = results[0, max_sharpe_ratio], results[1, max_sharpe_ratio]
    max_sharpe_allocation = pd.DataFrame(weights[max_sharpe_ratio],index=holding_tickers, columns=['allocation'])
    max_sharpe_allocation.allocation = [round(i * 100, 2) for i in max_sharpe_allocation.allocation]
    max_sharpe_allocation = max_sharpe_allocation.T

    min_vol = np.argmin(results[0])
    sdp_min, rp_min = results[0, min_vol], results[1, min_vol]
    min_vol_allocation = pd.DataFrame(weights[min_vol],index=holding_tickers, columns=['allocation'])
    min_vol_allocation.allocation = [round(i * 100, 2) for i in min_vol_allocation.allocation]
    min_vol_allocation = min_vol_allocation.T

    print('-' * 65)
    print('Maximum Sharpe Ratio Portfolio Allocation\n')
    print('Annualised Return:', round(rp, 2))
    print('Annualised Volatility:', round(sdp, 2))
    print('\n')
    print(max_sharpe_allocation)
    print('-' * 65)
    print('Minimum Volatility Portfolio Allocation\n')
    print('Annualised Return:', round(rp_min, 2))
    print('Annualised Volatility:', round(sdp_min, 2))
    print('\n')
    print(min_vol_allocation)
    plt.figure(figsize=(10, 7))
    plt.scatter(results[0, :], results[1, :], c=results[2, :], cmap='YlGnBu', marker='o', s=10, alpha=0.3)
    plt.colorbar()
    plt.scatter(sdp, rp, marker='*', color='r', s=400, label='Maximum Sharpe ratio')
    plt.scatter(sdp_min, rp_min, marker='*', color='g', s=400, label='Minimum volatility')
    plt.title('Simulated Portfolio Optimization based on Efficient Frontier')
    plt.xlabel('annualised volatility')
    plt.ylabel('annualised returns')
    plt.legend(labelspacing=0.8)
    plt.show()
print(efficient_frontier(mean_returns,cov_matrix,num_of_portfolios,risk_free_rate))
